import React from 'react'
import { AuthRouter } from './AuthRouter'
import {
    BrowserRouter as Router,
    Switch,
    Redirect,
  } from "react-router-dom";
import { NavogasRoutes } from './NavogasRoutes';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';
import { Backdrop, CircularProgress } from '@mui/material';
import { useSelector } from 'react-redux';
import { startChecking } from '../actions/auth';
//import { startLoadingNotes } from '../actions/notes';

export const AppRouter = () => {

    const dispatch = useDispatch();
    const { checking, uid } = useSelector(state => state.auth);

    useEffect(() => {
        dispatch( startChecking() );
    }, [dispatch])

    if ( checking ) {
        return (
            // <h1>Wait...</h1>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={checking}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
        )
    }

    return (
        <Router>
            <div>
                <Switch>
                    <PublicRoute 
                        isAuthenticated={ !!uid }
                        path="/auth"
                        component={ AuthRouter }
                    />
                    <PrivateRoute
                        isAuthenticated={ !!uid }
                        path="/"
                        component={ NavogasRoutes }
                    />
                    <Redirect to="/auth/login" />
                </Switch>
            </div>
        </Router>
    )
}
