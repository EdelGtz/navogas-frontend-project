import React, { useEffect, useState } from 'react';
import { Switch, Route, Redirect, useLocation } from "react-router-dom"
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';

import CssBaseline from '@mui/material/CssBaseline';

import { Sidebar } from '../components/main/Sidebar';
import { MainAppBar } from '../components/main/AppBar';
import { VentasScreen } from '../components/ventas/VentasScreen';
import { ReportesScreen } from '../components/reportes/ReportesScreen';
import { AutorizacionScreen } from '../components/autorizacion/AutorizacionScreen';
import { ClientesScreen } from '../components/clientes/ClientesScreen';
import { useDispatch } from 'react-redux';
import { clearActiveCliente } from '../actions/clientes';

const drawerWidth = 240;

const openedMixin = (theme) => ({
    width: drawerWidth,
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(9)} + 1px)`,
    },
});

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        ...(open && {
            ...openedMixin(theme),
            '& .MuiDrawer-paper': openedMixin(theme),
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
    }),
);

export const NavogasRoutes = () => {
    const theme = useTheme();

    const { pathname } = useLocation();

    const [open, setOpen] = useState(true);

    const dispatch = useDispatch();

    useEffect(() => {
        if ( pathname === "/clientes") {
            dispatch(clearActiveCliente());
        }
    }, [dispatch, pathname])

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Box sx={{ display: 'flex' }}>
                
                <CssBaseline />

                <MainAppBar open={open} handleDrawerOpen={handleDrawerOpen} AppBar={AppBar} />

                <Sidebar open={open} handleDrawerClose={handleDrawerClose} theme={theme} Drawer={Drawer} DrawerHeader={DrawerHeader} />

                <div style={{width:"100%"}}>
                    <Switch>

                        <Route exact path="/ventas" component={VentasScreen} />
                        <Route exact path="/reportes" component={ ReportesScreen } />
                        <Route exact path="/autorizacion" component={ AutorizacionScreen } />
                        <Route exact path="/clientes" component={ ClientesScreen } />
                        {/* <Route exact path="/hero/:heroeId" component={ HeroeScreen } /> */}

                        <Redirect to="/ventas" />
                    </Switch>
                </div>

            </Box>
        </>
    );
}