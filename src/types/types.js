export const types = {
    authLogin: '[Auth] Login',
    authLogout: '[Auth] Logout',
    authChecking: '[auth] Checking login state',
    authCheckingFinish: '[auth] Finish checking login state',
    authStartLogin: '[auth] Start login',    
    authStartTokenRenew: '[auth] Start token renew',

    clienteStartRegister: '[cliente] Start register',
    clienteStartUpdate: '[cliente] Start update',
    clienteStartDelete: '[cliente] Start delete',
    clienteLoad: '[cliente] Start load',
    clienteClearActive: '[cliente] Clear active client',

    uiSetError: '[UI] Set error',
    uiRemoveError: '[UI] Remove error',
    uiStartLoading: '[UI] Start loading',
    uiFinishLoading: '[UI] Finish loading',    

    gasGetPrice: '[GAS] Get gas price',
    gasUpdatePrice: '[GAS] Update gas price',

    pipasStartLoad: '[PIPA] Get pipas information',
    metodosPagoStartLoad: '[METODOPAGO] Get metodos pago information',

    ventasStartRegister: '[VENTA] Start register',
    ventasPendientesLoad: '[VENTA] Start load pendientes',
}