import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCGbQzgcudp4LZvQ-ASUKqz9gKpPL1XnRA",
    authDomain: "calentura-app-f3806.firebaseapp.com",
    projectId: "calentura-app-f3806",
    storageBucket: "calentura-app-f3806.appspot.com",
    messagingSenderId: "822205340179",
    appId: "1:822205340179:web:2becc1d51b891946abbde6"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);


const db = firebase.firestore();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export {
    db,
    googleAuthProvider,
    firebase
}