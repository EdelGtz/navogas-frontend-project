import { types } from "../types/types";


const initialState = {
    pipas: []
}

export const pipasReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        case types.pipasStartLoad:
            return {
                ...state,
                pipas: [ ...action.payload ]
            }
    
        default:
            return state;
    }

}