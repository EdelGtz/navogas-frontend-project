import { types } from "../types/types";


const initialState = {
    
}

export const gasReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        case types.gasGetPrice:
            return {
                ...state,
                ...action.payload
            }
    
        default:
            return state;
    }

}