import { types } from "../types/types";

const initialState = {
    active: null,
}

export const ventasReducer = ( state = initialState, action ) => {

    switch ( action.type ) {

        case types.ventasPendientesLoad:
            return {
                ...state,
                active: action.payload,
            }
        
        case types.ventasStartRegister:
            return {
                ...state,
                ...action.payload,
            }

        // case types.clienteStartDelete:
        //     return {
        //         ...initialState
        //     }
        // case types.clienteClearActive:
        //     return {
        //         ...initialState
        //     }
    
        default:
            return state;
    }

}