import { types } from "../types/types";

const initialState = {
    activeCliente: null,
}

export const clientesReducer = ( state = initialState, action ) => {

    switch ( action.type ) {

        case types.clienteLoad:
            return {
                ...state,
                activeCliente: action.payload,
            }
        
        case types.clienteStartUpdate:
            return {
                ...state,
                ...action.payload,
            }

        case types.clienteStartDelete:
            return {
                ...initialState
            }
        case types.clienteClearActive:
            return {
                ...initialState
            }
    
        default:
            return state;
    }

}