import { types } from "../types/types";


const initialState = {
    metodosPago: []
}

export const metodosPagoReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        case types.metodosPagoStartLoad:
            return {
                ...state,
                metodosPago: [ ...action.payload ]
            }
    
        default:
            return state;
    }

}