import { fetchConToken } from "../helpers/fetch";
import { types } from "../types/types";

export const pipasStartLoading = () => {
    return async( dispatch ) => {

        try {
            
            const resp = await fetchConToken( 'pipa' );
            const body = await resp.json();

            //console.log(body);

            dispatch( pipasStartLoad( body ) );

        } catch (error) {
            console.log(error)
        }

    }
}

const pipasStartLoad = ( values ) => ({
    type: types.pipasStartLoad,
    payload: values
})