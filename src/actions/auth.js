import Swal from "sweetalert2";
import { fetchConToken, fetchSinToken } from "../helpers/fetch"
import { types } from "../types/types";
import { finishLoading, startLoading } from "./ui";
//import { eventLogout } from "./events";

export const startLogin = ( username, password ) => {
    return async( dispatch ) => {
        
        dispatch( startLoading() );
        
        const resp = await fetchSinToken( 'auth/login', { username, password }, 'POST' );
        const body = await resp.json();

        if( body.accessToken ) {
            localStorage.setItem( 'token', body.accessToken );
            localStorage.setItem( 'token-init-date', new Date().getTime() );

            dispatch( login({
                uid: body.uid,
                name: body.name
            }));
            dispatch( finishLoading() ); 
        } else {
            Swal.fire('Error', body.message, 'error');
            dispatch( finishLoading() ); 
        }
    }
}

export const startChecking = () => {
    return async( dispatch ) => {
        const resp = await fetchConToken( 'auth/renew' );
        const body = await resp.json();

        //console.log(body);

        if( body.accessToken ) {
            localStorage.setItem( 'token', body.accessToken );
            localStorage.setItem( 'token-init-date', new Date().getTime() );

            dispatch( login({
                uid: body.uid,
                name: body.name
            }));
        } else {
            // Swal.fire('Error', body.msg, 'error');
            dispatch( checkingFinish() );
        }
    }
}

const checkingFinish = () => ({ type: types.authCheckingFinish });

const login = ( user ) => ({
    type: types.authLogin,
    payload: user
});

export const startLogout = () => {
    return ( dispatch ) => {
        localStorage.clear();
        //dispatch( eventLogout() );
        dispatch( logout() );
    }
}

const logout = () => ({
    type:types.authLogout
})