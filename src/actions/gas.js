//import Swal from "sweetalert2";
import moment from "moment";
import { fetchConToken } from "../helpers/fetch";
import { types } from "../types/types";

export const gasStartAddNew = ( gasInfo ) => {
    return async( dispatch ) => {

        const resp = await fetchConToken('precio-gas', gasInfo, 'POST');
            const body = await resp.json();

        try {            

            if ( body.precio ) {
                const values = {
                    precio: body.precio,
                    updatedAt: moment( body.updatedAt ).format('DD/MM/YYYY')
                }
                dispatch( getGetData( values ) );
            } else {
                console.log(body.message)
            }
            
        } catch (error) {            
            console.log(error);
        }

    }
}

export const gasStartLoading = () => {
    return async( dispatch ) => { 

        try {
            
            const resp = await fetchConToken( 'precio-gas' );
            const body = await resp.json();

            const values = {
                precio: body.precio,
                updatedAt: moment( body.updatedAt ).format('DD/MM/YYYY')
            }
            dispatch( getGetData( values ) );

        } catch (error) {
            console.log(error)
        }

    }
}

const getGetData = ( gasInfo ) => ({
    type: types.gasGetPrice,
    payload: gasInfo
});