import Swal from "sweetalert2";
import { fetchConToken } from "../helpers/fetch";
import { types } from "../types/types";
import { finishLoading, startLoading } from "./ui";

export const ventasStartRegister = (values) => {
  return async (dispatch) => {
    dispatch(startLoading());

    try {
      const resp = await fetchConToken("venta", values, "POST");
      const body = await resp.json();
      //console.log(body)
      if (body.id) {
        Swal.fire("Operación exitosa", "Venta realizada con éxito", "success");
        dispatch(finishLoading());
      } else {
        Swal.fire("Error", body.message, "error");
        dispatch(finishLoading());
        console.log(body.message);
      }
    } catch (error) {
      dispatch(finishLoading());
      console.log(error);
    }
  };
};

export const sendToAutorizacion = (values) => {
  return async (dispatch) => {
    dispatch(startLoading());

    try {
      const resp = await fetchConToken("venta/cambiar-estado", values, "PATCH");
      const body = await resp.json();
      //console.log(body)
      if (body.count > 0) {
        Swal.fire("Operación exitosa", "Ventas enviadas a bandeja de autorización", "success");
        dispatch(finishLoading());
      } else {
        Swal.fire("Error", body.message, "error");
        dispatch(finishLoading());
        console.log(body.message);
      }
    } catch (error) {
      dispatch(finishLoading());
      console.log(error);
    }
  };
};

export const autorizarVentas = (values) => {
  return async (dispatch) => {
    dispatch(startLoading());

    try {
      const resp = await fetchConToken("venta/cambiar-estado", values, "PATCH");
      const body = await resp.json();
      //console.log(body)
      if (body.count > 0) {
        Swal.fire("Operación exitosa", "Ventas autorizadas con éxito", "success");
        dispatch(finishLoading());
      } else {
        Swal.fire("Error", body.message, "error");
        dispatch(finishLoading());
        console.log(body.message);
      }
    } catch (error) {
      dispatch(finishLoading());
      console.log(error);
    }
  };
};

export const autorizarAbonos = (values) => {
  return async (dispatch) => {
    dispatch(startLoading());

    try {
      const resp = await fetchConToken("abono/cambiar-estado", values, "PATCH");
      const body = await resp.json();
      //console.log(body)
      if (body.count > 0) {
        Swal.fire("Operación exitosa", "Abonos autorizados con éxito", "success");
        dispatch(finishLoading());
      } else {
        Swal.fire("Error", body.message, "error");
        dispatch(finishLoading());
        console.log(body.message);
      }
    } catch (error) {
      dispatch(finishLoading());
      console.log(error);
    }
  };
};

export const ventasPorPagarById = (values) => {
  return async (dispatch) => {
    try {
      const resp = await fetchConToken(
        `venta/pendiente?estadoOperacion=AUTORIZADA&tipoVenta=CREDITO&clienteId=${values}`
      );
      const body = await resp.json();
      //console.log(body);
      if (body) {
        dispatch(ventasLoad(body));
      } else {
        //Swal.fire('Atención', "No se encontro cliente con ese ID", 'warning')
        dispatch(finishLoading());
        console.log(body.message);
      }
    } catch (error) {
      console.log(error);
    }
  };
};

const ventasLoad = (ventas) => ({
  type: types.ventasPendientesLoad,
  payload: ventas,
});

export const abonoStartNew = (values) => {
  return async (dispatch) => {
    dispatch(startLoading());

    try {
      const resp = await fetchConToken("abono", values, "POST");
      const body = await resp.json();
      console.log(body);
      if (body.id) {
        Swal.fire("Operación exitosa", "Abono realizado con éxito", "success");
        dispatch(finishLoading());
      } else {
        Swal.fire("Error", body.message, "error");
        dispatch(finishLoading());
        console.log(body.message);
      }
    } catch (error) {
      dispatch(finishLoading());
      console.log(error);
    }
  };
};
