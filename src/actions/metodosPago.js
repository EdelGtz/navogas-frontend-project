import { fetchConToken } from "../helpers/fetch";
import { types } from "../types/types";

export const metodosPagoStartLoading = () => {
    return async( dispatch ) => {

        try {
            
            const resp = await fetchConToken( 'metodo-pago' );
            const body = await resp.json();

            //console.log(body);

            dispatch( metodosPagoStartLoad( body ) );

        } catch (error) {
            console.log(error)
        }

    }
}

const metodosPagoStartLoad = ( values ) => ({
    type: types.metodosPagoStartLoad,
    payload: values
})