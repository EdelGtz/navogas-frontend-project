import Swal from "sweetalert2";
import { fetchConToken } from "../helpers/fetch";
import { types } from "../types/types";
import { finishLoading, startLoading } from "./ui";

export const clientesStartAddNew = ( values ) => {
    return async( dispatch ) => {

        dispatch( startLoading() );

        try {            

            const resp = await fetchConToken('cliente', values, 'POST');
            const body = await resp.json();
            //console.log(body)
            if ( body.id ) {
                Swal.fire('Operación exitosa', 'Cliente agregado con éxito', 'success')
                dispatch( finishLoading() );
            } else {
                Swal.fire('Error', body.message, 'error')
                dispatch( finishLoading() );
                console.log(body.message)
            }
            
        } catch (error) {        
            dispatch( finishLoading() );
            console.log(error);
        }

    }
}

export const clientesSearch = ( idCliente ) => {
    return async( dispatch ) => {

        try {            

            const resp = await fetchConToken(`cliente/${ idCliente }`, idCliente);
            const body = await resp.json();
            //console.log(body)
            if ( body.nombre ) {                
                dispatch( clienteLoad( body ) );
            } else {
                Swal.fire('Atención', "No se encontro cliente con ese ID", 'warning')
                dispatch( finishLoading() );
                console.log(body.message)
            }
            
        } catch (error) {        
            console.log(error);
        }

    }
}

export const clientesSearchByName = ( nombre ) => {
    return async( dispatch ) => {

        try {            

            const resp = await fetchConToken(`cliente?nombre=${ nombre }`);
            const body = await resp.json();
            //console.log(body[0]);
            if ( body[0].nombre ) {                
                dispatch( clienteLoad( body[0] ) );
            } else {
                Swal.fire('Atención', "No se encontro cliente con ese ID", 'warning')
                dispatch( finishLoading() );
                console.log(body.message)
            }
            
        } catch (error) {        
            console.log(error);
        }

    }
}

const clienteLoad = ( cliente ) => ({
    type: types.clienteLoad,
    payload: cliente
})

export const clearActiveCliente = () => ({
    type: types.clienteClearActive
})

export const clientesStartUpdate = ( values ) => {
    return async( dispatch ) => {

        dispatch( startLoading() );

        try {            

            const resp = await fetchConToken( `cliente/${ values.id }`, values, 'PATCH' );
            const body = await resp.json();
            //console.log(body)
            if ( body.id ) {
                Swal.fire('Operación exitosa', 'Cliente actualizado con éxito', 'success')
                dispatch( finishLoading() );
            } else {
                Swal.fire('Error', body.message, 'error')
                dispatch( finishLoading() );
                console.log(body.message)
            }
            
        } catch (error) {        
            dispatch( finishLoading() );    
            console.log(error);
        }

    }
}