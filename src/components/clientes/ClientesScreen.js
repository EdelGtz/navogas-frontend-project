import React, { useEffect, useState } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import LoadingButton from '@mui/lab/LoadingButton';
import TextField from '@mui/material/TextField';
import { IconButton, Toolbar } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import SaveIcon from '@mui/icons-material/Save';
import { useForm } from '../../hooks/useForm';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import validator from 'validator';
import { styled } from '@mui/material/styles';
import ClearAllIcon from '@mui/icons-material/ClearAll';

import { clearActiveCliente, clientesSearch, clientesStartAddNew, clientesStartUpdate } from '../../actions/clientes';


const theme = createTheme();

const LimpiarButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText("#ef6c00"),
    backgroundColor: "#ef6c00",
    '&:hover': {
      backgroundColor: "#e65100",
    },
  }));

const initCliente = {
    nombre: '',
    direccion: '',
    celular: '',
    descuento: 0
}

export const ClientesScreen = () => {

    const dispatch = useDispatch();

    const [msgError, setMsgError] = useState(false);

    const { loading } = useSelector(state => state.ui)

    const { activeCliente } = useSelector(state => state.clientes)

    const [formValues, setFormValues] = useState(initCliente);

    const { nombre, direccion, celular, descuento } = formValues;

    useEffect(() => {
        if (activeCliente) {
            setFormValues(activeCliente);
        } else {
            setFormValues(initCliente);
        }        
    }, [activeCliente, setFormValues])

    const handleRegisterInputChange = ({ target }) => {
        setFormValues({
            ...formValues,
            [target.name]: target.value
        })
    }

    const [formSearch, handleInputChangeSearch, resetIdCliente] = useForm({
        idCliente: ''
    });

    const { idCliente } = formSearch;

    const handleSearch = (e) => {
        e.preventDefault();
        dispatch(clientesSearch(idCliente))
    }

    const handleRegister = () => {

        if (validator.isEmpty(nombre) || validator.isEmpty(direccion) || validator.isEmpty(celular) || validator.isEmpty(descuento)) {
            setMsgError("Los campos marcados con (*) son obligatorios y deben ser llenados");
        } else {

            if ( activeCliente ) {
                const values = {
                    id: idCliente,
                    nombre,
                    direccion,
                    celular,
                    descuento: parseFloat(descuento)
                }
    
                dispatch(clientesStartUpdate(values));
                dispatch(clearActiveCliente());
                setFormValues(initCliente);
                setMsgError('');
                resetIdCliente();   
                
            } else {
                const values = {
                    nombre,
                    direccion,
                    celular,
                    descuento: parseFloat(descuento)
                }
    
                dispatch(clientesStartAddNew(values));
                dispatch(clearActiveCliente());
                setFormValues(initCliente);
                setMsgError('');
                resetIdCliente();                
            }            
        }
    }

    const handleLimpiarDatos = () => {
        dispatch(clearActiveCliente());
        setFormValues(initCliente);
        setMsgError('');
        resetIdCliente();
    }

    return (
        <ThemeProvider theme={theme}>
            <Toolbar />
            <CssBaseline />
            <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
                <Paper variant="outlined" sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
                    <React.Fragment>
                        {
                            activeCliente
                                ?
                                <Typography variant="h5" gutterBottom>
                                    Editar cliente
                                </Typography>
                                :
                                <Typography variant="h5" gutterBottom>
                                    Agregar cliente
                                </Typography>
                        }

                        <Grid container spacing={3}>
                            <Grid item xs={10}>
                                <TextField
                                    id="idCliente"
                                    name="idCliente"
                                    value={idCliente}
                                    onChange={handleInputChangeSearch}
                                    label="ID Cliente"
                                    fullWidth
                                    autoComplete="off"
                                    variant="standard"
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <IconButton aria-label="search" size="large" onClick={ handleSearch }>
                                    <SearchIcon />
                                </IconButton>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    id="firstName"
                                    name="nombre"
                                    value={nombre}
                                    onChange={handleRegisterInputChange}
                                    label="Nombre"
                                    fullWidth
                                    autoComplete="off"
                                    variant="standard"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    id="address1"
                                    name="direccion"
                                    value={direccion}
                                    onChange={handleRegisterInputChange}
                                    label="Dirección"
                                    fullWidth
                                    autoComplete="off"
                                    variant="standard"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    id="celular"
                                    name="celular"
                                    value={celular}
                                    onChange={handleRegisterInputChange}
                                    label="Celular"
                                    fullWidth
                                    autoComplete="off"
                                    variant="standard"
                                />
                            </Grid>
                            {/* <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    id="precioLitro"
                                    type="number"
                                    name="precioLitro"
                                    label="Precio por litro"
                                    fullWidth
                                    autoComplete="off"
                                    variant="standard"
                                />
                            </Grid> */}
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    id="descuento"
                                    type="number"
                                    name="descuento"
                                    value={descuento}
                                    onChange={handleRegisterInputChange}
                                    label="Descuento"
                                    autoComplete="off"
                                    fullWidth
                                    variant="standard"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                {
                                    msgError &&
                                    <Typography variant="caption" display="block" gutterBottom sx={{ color: "red", pl: 2, pb: 2 }}>
                                        {msgError}
                                    </Typography>
                                }
                            </Grid>
                            <Grid item xs={4}>
                            </Grid>
                            <Grid item xs={5}>
                                <LimpiarButton variant="outlined" startIcon={<ClearAllIcon />} color="error" onClick={handleLimpiarDatos}>
                                    Limpiar campos
                                </LimpiarButton>
                            </Grid>
                            <Grid item xs={3}>
                                <LoadingButton
                                    color="primary"
                                    onClick={handleRegister}
                                    loading={loading}
                                    loadingPosition="start"
                                    startIcon={<SaveIcon />}
                                    variant="contained"
                                >
                                    Guardar
                                </LoadingButton>
                            </Grid>
                        </Grid>
                    </React.Fragment>
                </Paper>
            </Container>
        </ThemeProvider>
    );
}