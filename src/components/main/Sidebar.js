import React, { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import { IconButton, Modal, Stack, TextField } from '@mui/material';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import BarChartIcon from '@mui/icons-material/BarChart';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import GroupAddIcon from '@mui/icons-material/GroupAdd';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import { styled } from '@mui/material/styles';
import { lightBlue } from '@mui/material/colors';
//import useSWR from 'swr';

import logo from '../../assets/navogas.svg';
import '../../styles/Sidebar.css';
import Box from '@mui/material/Box'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import { gasStartAddNew, gasStartLoading } from '../../actions/gas';
import validator from 'validator';
import moment from 'moment';

const modalStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(lightBlue["A700"]),
  backgroundColor: lightBlue["A700"],
  '&:hover': {
    backgroundColor: lightBlue["600"],
  },
}));

export const Sidebar = ({ open, handleDrawerClose, theme, Drawer, DrawerHeader }) => {

  //const { data } = useSWR('/precio-gas', {refreshInterval:1000});   
  const { precio, updatedAt} = useSelector(state => state.gas);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!precio) {
      dispatch( gasStartLoading() );
    }
  }, [dispatch, precio ])

  const { pathname } = useLocation();
  const [openModal, setOpenModal] = useState(false);
  const handleOpen = () => setOpenModal(true);

  // if (!data) {
  //   return <h1>Cargando...</h1>
  // }

  // const { precio, updatedAt} = data;

  return (
    <Drawer variant="permanent" open={open}>
      <DrawerHeader>
        <div className="center-logo">
          <Link to="/">
            <img className="sidebar__twitterIcon" width="175" src={logo} alt={"logo"} />
          </Link>
        </div>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
        </IconButton>
      </DrawerHeader>
      <Divider />
      <ListItem
        component={Link}
        to="/ventas"
        button
        selected={pathname === "/ventas"}
      >
        <ListItemIcon>
          <MonetizationOnIcon />
        </ListItemIcon>
        <ListItemText primary="Registro de ventas" />
      </ListItem>
      <ListItem
        component={Link}
        to="/reportes"
        button
        selected={pathname === "/reportes"}
      >
        <ListItemIcon>
          <BarChartIcon />
        </ListItemIcon>
        <ListItemText primary="Reportes" />
      </ListItem>
      <ListItem
        component={Link}
        to="/autorizacion"
        button
        selected={pathname === "/autorizacion"}
      >
        <ListItemIcon>
          <CheckCircleIcon />
        </ListItemIcon>
        <ListItemText primary="Panel de autorización" />
      </ListItem>

      {open &&
        <div style={{ width: "100%", position: "absolute", bottom: "0", marginBottom: "100px" }}>
          <div style={{ textAlign: "center" }}>
            <Card elevation={0}>
              <CardContent sx={{ scrollPaddingBottom: 0 }}>
                <Typography variant="h6" component="div">
                  Precio por litro Gas
                </Typography>
                <Typography variant="h3" component="div">
                  {precio}
                </Typography>
                <Typography variant="body2">
                  Actualizado el: {moment(updatedAt).format('DD/MM/YYYY')}
                </Typography>
              </CardContent>
              <ColorButton onClick={handleOpen} variant="contained" size="small">Actualizar</ColorButton>
            </Card>
          </div>
        </div>
      }

      <div style={{ width: "100%", position: "absolute", bottom: "0", }}>
        <Divider />
        <ListItem
          component={Link}
          to="/clientes"
          button
          sx={{ marginBottom: "10px", marginTop: "10px" }}
          selected={pathname === "/clientes"}
        >
          <ListItemIcon>
            <GroupAddIcon />
          </ListItemIcon>
          <ListItemText primary="Agregar cliente" />
        </ListItem>
      </div>

      <ModalActualizar openModal={openModal} setOpenModal={setOpenModal} precioGas={precio} />
    </Drawer>
  )
}

const ModalActualizar = ({ openModal, setOpenModal, precioGas }) => {

  const dispatch = useDispatch();

  const [msgError, setMsgError] = useState(false);

  const [formValues, handleInputChange] = useForm({
    precio: precioGas,
  });
  const { precio } = formValues;

  const handleClose = () => {
    setOpenModal(false);
    setMsgError(false);
  }


  const handleUpdate = () => {

    if (typeof (precio) === "undefined" || validator.isEmpty(precio)) {
      setMsgError("Debe ingresar un precio para el litro de gas");
    } else {
      const values = {
        precio: parseFloat(precio)
      }

      dispatch(gasStartAddNew(values));
      setOpenModal(false);
    }
  }

  return (
    <div>
      <Modal
        open={ openModal }
        onClose={ handleClose }
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={ modalStyle }>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Precio por litro Gas actual
          </Typography>
          <div style={{ textAlign: "center" }}>
            <TextField
              id="standard-basic"
              type="number"
              variant="standard"
              name="precio"
              value={ precio }
              onChange={ handleInputChange }
              inputProps={{ style: { fontSize: 40 } }}
              sx={{ mt: 4, mb: 4, width: 105 }}
            />
          </div>

          {
            msgError &&
            <Typography variant="caption" display="block" gutterBottom sx={{ color: "red", pl: 2, pb: 2 }}>
              {msgError}
            </Typography>
          }

          <Divider />
          <Stack direction="row" spacing={2} sx={{ mt: 1 }}>
            <Button variant="outlined" color="error" onClick={ handleClose }>
              Cancelar
            </Button>
            <Button variant="contained" color="success" onClick={ handleUpdate }>
              Actualizar
            </Button>
          </Stack>
        </Box>
      </Modal>
    </div>
  );
}
