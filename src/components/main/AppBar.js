import React, { useState } from 'react'
import Toolbar from '@mui/material/Toolbar';

import Typography from '@mui/material/Typography';

import LogoutIcon from '@mui/icons-material/Logout';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Avatar, Menu, MenuItem } from '@mui/material';
import { stringAvatar } from '../../helpers/stringAvatar';
import { useDispatch } from 'react-redux';
import { startLogout } from '../../actions/auth';
import { useSelector } from 'react-redux';


export const MainAppBar = ({ open, handleDrawerOpen, AppBar }) => {

    const { name } = useSelector(state => state.auth);

    const dispatch = useDispatch();

    const [anchorEl, setAnchorEl] = useState(null);
    const openMenu = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        dispatch( startLogout() );
    };

    return (
        <AppBar position="fixed" open={open} style={{ background: '#043568' }}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    sx={{
                        marginRight: '36px',
                        ...(open && { display: 'none' }),
                    }}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" noWrap component="div">
                    
                </Typography>

                <div style={{marginLeft: "auto", marginRight: 0}}>
                    <Avatar {...stringAvatar(name)} onClick={handleClick} />
                    <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={openMenu}
                        onClose={handleClose}
                        MenuListProps={{
                        'aria-labelledby': 'basic-button',
                        }}
                    >
                        <MenuItem onClick={handleLogout}>
                            <LogoutIcon />
                            Logout
                        </MenuItem>
                    </Menu>
                </div>
                
            </Toolbar>
        </AppBar>
    )
}
