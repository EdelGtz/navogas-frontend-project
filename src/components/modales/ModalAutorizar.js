import React from "react";
import { Button, Divider, Modal, Stack, Typography } from "@mui/material";
import Box from "@mui/material/Box";

const modalStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

export const ModalAutorizar = ({ openModal, setOpenModal, modalMessage, onSubmit }) => {
  const handleClose = () => setOpenModal(false);

  const handleSubmit = () => {
    handleClose();
    onSubmit();
  };

  return (
    <div>
      <Modal
        open={openModal}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            {modalMessage.header}
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2, mb: 1 }}>
            {modalMessage.body}
          </Typography>
          <Divider />
          <Stack direction="row" spacing={2} sx={{ mt: 1 }}>
            <Button variant="outlined" color="error" onClick={handleClose}>
              Cancelar
            </Button>
            <Button variant="contained" color="success" onClick={handleSubmit}>
              Enviar
            </Button>
          </Stack>
        </Box>
      </Modal>
    </div>
  );
};
