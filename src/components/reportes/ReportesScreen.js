import { Toolbar, Typography } from '@mui/material'
import React, { useState } from 'react'
import { Filtros } from './Filtros'
import { Grafica } from './Grafica'
import { Reporte } from './Reporte'

export const ReportesScreen = () => {

    const [reporte, setReporte] = useState(true);

    return (
        <div>
            <Toolbar />
            <Typography sx={{ margin:2 }} variant="h5" gutterBottom component="div">
                Reportes
            </Typography>
            
            <Filtros reporte={reporte} setReporte={setReporte} />

            {
                reporte ?
                    <Reporte />
                    :
                    <Grafica />
            }           

        </div>
    )
}
