import React, { useState } from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Grid, IconButton, TextField } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import BarChartIcon from '@mui/icons-material/BarChart';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { es } from "date-fns/locale";


export const Filtros = ({ reporte, setReporte }) => {

    const [value, setValue] = useState('pagadas');
    //const [locale, setLocale] = useState("es");

    const [fechaInicio, setFechaInicio] = useState(null);
    const [fechaFin, setFechaFin] = useState(null);

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container columns={16}>
                <Grid >
                    <Card sx={{ minWidth: 160 }} elevation={0}>
                        <CardContent>
                            <Typography color="text.primary">
                                ID Cliente
                            </Typography>
                            <TextField id="outlined-size-small" size="small" />
                        </CardContent>
                    </Card>
                </Grid>
                <Grid >
                    <Card sx={{ minWidth: 160 }} elevation={0}>
                        <CardContent>
                            <Typography color="text.primary">
                                Nombre de cliente
                            </Typography>
                            <TextField id="outlined-size-small" size="small" />
                        </CardContent>
                    </Card>
                </Grid>
                <Grid >
                    <Card sx={{ minWidth: 160 }} elevation={0}>
                        <CardContent>
                            <Typography color="text.primary">
                                Fecha Inicio
                            </Typography>
                            <LocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
                                <DatePicker
                                    value={fechaInicio}
                                    onChange={(newValue) => {
                                        setFechaInicio(newValue);
                                    }}
                                    renderInput={(params) => <TextField {...params} size="small" />}
                                />
                            </LocalizationProvider>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid >
                    <Card sx={{ minWidth: 160 }} elevation={0}>
                        <CardContent>
                            <Typography color="text.primary">
                                Fecha fin
                            </Typography>
                            <LocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
                                <DatePicker
                                    value={fechaFin}
                                    onChange={(newValue) => {
                                        setFechaFin(newValue);
                                    }}
                                    renderInput={(params) => <TextField {...params} size="small" />}
                                />
                            </LocalizationProvider>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid >
                    <Card sx={{ minWidth: 160 }} elevation={0}>
                        <CardContent>
                            <RadioGroup
                                aria-label="gender"
                                name="controlled-radio-buttons-group"
                                value={value}
                                onChange={handleChange}
                            >
                                <FormControlLabel
                                    value="pagadas"
                                    control={<Radio />}
                                    sx={{
                                        '& .MuiSvgIcon-root': {
                                            fontSize: 15,
                                        },
                                    }}
                                    label="Pagadas"
                                />
                                <FormControlLabel
                                    value="pendientes"
                                    control={<Radio />}
                                    sx={{
                                        '& .MuiSvgIcon-root': {
                                            fontSize: 15,
                                        },
                                    }}
                                    label="Pendientes"
                                />
                            </RadioGroup>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid>
                    <Card sx={{ minWidth: 5 }} elevation={0}>
                        <CardContent sx={{ pl:0 }}>
                            <IconButton 
                                aria-label="search"
                                size="large"
                                sx={{
                                    padding: 0, '& svg': {
                                        fontSize: 50
                                    }
                                }}
                            >
                                <SearchIcon />
                            </IconButton>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid>
                    <Card sx={{ minWidth: 160 }} elevation={0}>
                        <CardContent>
                            <Typography color="text.primary">
                                Total ventas
                            </Typography>
                            <Typography variant="h6" component="div">
                                $11,341.00
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid>
                    <IconButton
                        color="primary"
                        sx={{
                            padding: 0, '& svg': {
                                fontSize: 80
                            }
                        }}
                        onClick={() => setReporte(!reporte)}
                    >
                        <BarChartIcon />
                    </IconButton>
                </Grid>

            </Grid>
        </Box>
    )
}
