import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DataGrid } from "@mui/x-data-grid";
import useSWR from "swr";
import moment from "moment";
import { Footer } from "./Footer";
import { autorizarAbonos } from "../../actions/ventas";
import Swal from "sweetalert2";

const columns = [
  { field: "id", headerName: "ID Abono", width: 100 },
  { field: "ventaId", headerName: "ID Venta", width: 100 },
  { field: "cantidad", headerName: "Cantidad", width: 100 },
  { field: "fecha", headerName: "Fecha", width: 100 },
  { field: "clienteId", headerName: "ID Cliente", width: 100 },
  { field: "nombre", headerName: "Nombre del cliente", width: 200 },
  { field: "metodoPago", headerName: "Metodo de pago", width: 130 },
];

const modalMessage = {
  header: "Autorización de abonos",
  body: "¿Estas seguro que deseas AUTORIZAR estos abonos?",
};

export const TabAbonos = () => {
  const dispatch = useDispatch();
  const { data: abonos } = useSWR("/abono?estadoOperacion=VERIFICACION", { refreshInterval: 1000 });

  useEffect(() => {
    console.log(abonos);
  }, [abonos]);

  const [rows, setRows] = useState([]);
  const [selectionModel, setSelectionModel] = useState([]);

  function handleAutorizar() {
    if (selectionModel.length > 0) {
      const data = {
        ids: selectionModel,
        estadoOperacion: "AUTORIZADA",
      };
      dispatch(autorizarAbonos(data));
    } else {
      Swal.fire("Atención", "Seleccione un abono para autorizar", "warning");
    }
  }

  useEffect(() => {
    if (abonos) {
      const rows = abonos.map((abono) => {
        const { venta } = abono;
        console.log(venta);
        return {
          id: abono.id,
          ventaId: venta.id,
          cantidad: Number(abono.cantidad).toFixed(2),
          fecha: moment(venta.createdAt).format("DD/MM/YYYY"),
          clienteId: venta.clienteId,
          nombre: venta.cliente.nombre,
          metodoPago: abono.metodoPago.denominacion,
        };
      });
      setRows(rows);
    }
  }, [abonos]);

  if (!abonos) {
    return <div>Cargando...</div>;
  }

  return (
    <>
      <div style={{ height: 600, width: "100%" }}>
        <DataGrid
          rows={rows}
          columns={columns}
          getRowId={(r) => r.id}
          pageSize={100}
          rowsPerPageOptions={[100]}
          checkboxSelection
          onSelectionModelChange={(newSelectionModel) => {
            setSelectionModel(newSelectionModel);
          }}
          selectionModel={selectionModel}
        />
      </div>
      {abonos && <Footer indicadores={{}} modalMessage={modalMessage} onAutorizar={handleAutorizar} />}
    </>
  );
};
