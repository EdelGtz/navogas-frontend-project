import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DataGrid } from "@mui/x-data-grid";
import useSWR from "swr";
import moment from "moment";
import { Footer } from "./Footer";
import { autorizarVentas } from "../../actions/ventas";
import Swal from "sweetalert2";
import { sum } from "mathjs";

const columns = [
  { field: "id", headerName: "ID Venta", width: 100 },
  { field: "fecha", headerName: "Fecha", width: 100 },
  { field: "clienteId", headerName: "ID Cliente", width: 100 },
  { field: "nombre", headerName: "Nombre del cliente", width: 200 },
  { field: "tipoVenta", headerName: "Tipo de venta", width: 130 },
  { field: "metodoPago", headerName: "Metodo de pago", width: 130 },
  { field: "litros", headerName: "Litros", width: 100 },
  { field: "precioLitro", headerName: "Precio por litro", width: 130 },
  { field: "venta", headerName: "Venta", width: 130 },
  { field: "anticipo", headerName: "Anticipo", width: 130 },
  { field: "pendiente", headerName: "Pendiente", width: 130 },
];

const modalMessage = {
  header: "Autorización de ventas",
  body: "¿Estas seguro que deseas AUTORIZAR estas ventas?",
};

export const TabPipa = ({ pipaId }) => {
  const dispatch = useDispatch();
  const { data: ventas } = useSWR(`/venta?estadoOperacion=AUTORIZACION&pipaId=${pipaId}`, { refreshInterval: 1000 });

  const [selectionModel, setSelectionModel] = useState([]);
  const [rows, setRows] = useState([]);
  const [indicadores, setIndicadores] = useState({});
  const [sendData, setSendData] = useState(false);

  function handleAutorizar() {
    if (selectionModel.length > 0) {
      const data = {
        ids: selectionModel,
        estadoOperacion: "AUTORIZADA",
      };
      dispatch(autorizarVentas(data));
    } else {
      Swal.fire("Atención", "Seleccione una venta para autorizar", "warning");
    }
  }

  useEffect(() => {
    if (ventas) {
      // Llenar tabla con ventas
      const rows = ventas.map((venta) => ({
        id: venta.id,
        fecha: moment(venta.createdAt).format("DD/MM/YYYY"),
        clienteId: venta.clienteId,
        nombre: venta.cliente.nombre,
        tipoVenta: venta.tipoVenta,
        metodoPago: venta.metodoPago.denominacion,
        litros: venta.litrosVendidos,
        precioLitro: venta.precioGas,
        venta: venta.monto,
        anticipo: venta.tipoVenta === "CONTADO" ? 0 : venta.abonos.find((abono) => abono.anticipo)?.cantidad || 0,
        pendiente:
          venta.tipoVenta === "CONTADO"
            ? 0
            : venta.monto - (venta.abonos.find((abono) => abono.anticipo)?.cantidad || 0),
      }));
      setRows(rows);

      // Calcular indicadores
      const ventasCredito = ventas.filter((venta) => venta.tipoVenta === "CREDITO");

      const totalVenta = sum(ventas.map((venta) => Number(venta.monto)));

      const totalAnticipo = sum(
        ventasCredito.map((venta) => Number(venta.abonos.find((abono) => abono.anticipo)?.cantidad || 0))
      );

      const totalPendiente = sum(
        ventasCredito.map(
          (venta) => Number(venta.monto) - Number(venta.abonos.find((abono) => abono.anticipo)?.cantidad || 0)
        )
      );

      const totalLitros = sum(ventas.map((venta) => venta.litrosVendidos));
      const totalContado = ventas.filter((venta) => venta.tipoVenta === "CONTADO").length;
      const totalCredito = ventas.filter((venta) => venta.tipoVenta === "CREDITO").length;

      const indicadores = {
        totalVenta,
        totalAnticipo,
        totalPendiente,
        totalLitros,
        totalContado,
        totalCredito,
      };
      setIndicadores(indicadores);
    }
  }, [ventas]);

  if (!ventas) {
    return <div>Cargando...</div>;
  }

  return (
    <div style={{ height: 600, width: "100%" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        getRowId={(r) => r.id}
        pageSize={100}
        rowsPerPageOptions={[100]}
        checkboxSelection
        onSelectionModelChange={(newSelectionModel) => {
          setSelectionModel(newSelectionModel);
        }}
        selectionModel={selectionModel}
      />
      {ventas && <Footer indicadores={indicadores} modalMessage={modalMessage} onAutorizar={handleAutorizar} />}
    </div>
  );
};
