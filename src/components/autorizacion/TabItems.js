import React, { useState } from 'react'
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { TabBP20 } from './TabBP20';
import { TabBP21 } from './TabBP21';
import { TabBP30 } from './TabBP30';
import { TabBP31 } from './TabBP31';
import { TabJacarandas } from './TabJacarandas';
import { TabAbonos } from './TabAbonos';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export const TabItems = () => {

    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Box sx={{ width: '100%' }}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} aria-label="tabs">
                    <Tab label="BP20" {...a11yProps(0)} />
                    <Tab label="BP21" {...a11yProps(1)} />
                    <Tab label="BP30" {...a11yProps(2)} />
                    <Tab label="BP31" {...a11yProps(3)} />
                    <Tab label="JACARANDAS" {...a11yProps(4)} />
                    <Tab label="Abonos" {...a11yProps(5)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <TabBP20 />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <TabBP21 />
            </TabPanel>
            <TabPanel value={value} index={2}>
                <TabBP30 />
            </TabPanel>
            <TabPanel value={value} index={3}>
                <TabBP31 />
            </TabPanel>
            <TabPanel value={value} index={4}>
                <TabJacarandas />
            </TabPanel>
            <TabPanel value={value} index={5}>
                <TabAbonos />
            </TabPanel>
        </Box>
    )
}
