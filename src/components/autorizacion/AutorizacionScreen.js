import { Toolbar, Typography } from '@mui/material'
import React from 'react'
import { TabItems } from './TabItems'

export const AutorizacionScreen = () => {
    return (
        <div>
            <Toolbar />
            <Typography sx={{ margin:2 }} variant="h5" gutterBottom component="div">
                Panel de Autorización
            </Typography>

            <TabItems />            
        </div>
    )
}
