import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Button, Grid } from "@mui/material";
import { styled } from "@mui/system";
import Box from "@mui/material/Box";
import { ModalAutorizar } from "../modales/ModalAutorizar";

const AutorizarButton = styled(Button)(({ theme }) => ({
  backgroundColor: "#043568",
  "&:hover": {
    backgroundColor: "#064990",
  },
  height: 80,
  width: 150,
}));

export const Footer = ({ indicadores, modalMessage, onAutorizar }) => {
  const [openModal, setOpenModal] = useState(false);
  const handleOpen = () => setOpenModal(true);
  const handleSubmit = () => onAutorizar();

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container columns={16}>
        <Grid>
          <AutorizarButton variant="contained" size="small" onClick={handleOpen}>
            Autorizar
          </AutorizarButton>
        </Grid>
        {!!indicadores.totalVenta && (
          <Grid>
            <Card sx={{ minWidth: 160 }} elevation={0}>
              <CardContent>
                <Typography color="text.primary">Total venta</Typography>
                <Typography variant="h6" component="div">
                  {`$${indicadores.totalVenta || 0}`}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        )}
        {!!indicadores.totalAnticipo && (
          <Grid>
            <Card sx={{ minWidth: 160 }} elevation={0}>
              <CardContent>
                <Typography color="text.primary">Total anticipo</Typography>
                <Typography variant="h6" component="div">
                  {`$${indicadores.totalAnticipo || 0}`}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        )}
        {!!indicadores.totalPendiente && (
          <Grid>
            <Card sx={{ minWidth: 160 }} elevation={0}>
              <CardContent>
                <Typography color="text.primary">Total pendiente</Typography>
                <Typography variant="h6" component="div">
                  {`$${indicadores.totalPendiente || 0}`}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        )}
        {!!indicadores.totalLitros && (
          <Grid>
            <Card sx={{ minWidth: 160 }} elevation={0}>
              <CardContent>
                <Typography color="text.primary">Total litros</Typography>
                <Typography variant="h6" component="div">
                  {indicadores.totalLitros || 0}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        )}
        {!!indicadores.totalContado && (
          <Grid>
            <Card sx={{ minWidth: 160 }} elevation={0}>
              <CardContent>
                <Typography color="text.primary">Ventas contado</Typography>
                <Typography variant="h6" component="div">
                  {indicadores.totalContado || 0}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        )}
        {!!indicadores.totalCredito && (
          <Grid>
            <Card sx={{ minWidth: 160 }} elevation={0}>
              <CardContent>
                <Typography color="text.primary">Ventas crédito</Typography>
                <Typography variant="h6" component="div">
                  {indicadores.totalCredito || 0}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        )}
      </Grid>

      <ModalAutorizar
        openModal={openModal}
        setOpenModal={setOpenModal}
        modalMessage={modalMessage}
        onSubmit={handleSubmit}
      />
    </Box>
  );
};
