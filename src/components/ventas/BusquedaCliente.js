import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { InputAdornment, TextField, Typography } from '@mui/material';
import { IconButton } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { clientesSearchByName, clientesSearch } from '../../actions/clientes';
import { ventasPorPagarById } from '../../actions/ventas';
//import { ReactSearchAutocomplete } from "react-search-autocomplete";


const initialformValues = {
    id:'',
    nombre: '',
    direccion: '',
    celular: '',
    descuento: ''
}

export const BusquedaCliente = () => {

    const dispatch = useDispatch();

    const { activeCliente } = useSelector(state => state.clientes)
    const { precio } = useSelector(state => state.gas)

    const [formValues, setFormValues] = useState(initialformValues);

    const {
        id,
        nombre,
        direccion,
        celular,
        descuento,
    } = formValues;

    useEffect(() => {
        if (activeCliente) {
            setFormValues(activeCliente);
        }
        
    }, [activeCliente, setFormValues]);    

    const handleInputChange = ({ target }) => {
        setFormValues({
            ...formValues,
            [target.name]: target.value
        })
    }

    const handleSearch = (e) => {
        e.preventDefault();
        if ( id ) {
            dispatch(clientesSearch( id ));
            dispatch(ventasPorPagarById( id ));
        } else if ( nombre ) {
            dispatch(clientesSearchByName( nombre ));
        }
    }

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={2} columns={16}>
                <Grid item xs={6}>
                    <Typography variant="h6" component="div">
                        Cliente
                    </Typography>
                    <Grid container spacing={2} columns={16} sx={{ mt:1 }}>
                        <Grid item xs={6}>
                            <TextField id="outlined-basic" name="id" value={ id } onChange={ handleInputChange } label="ID del cliente" variant="outlined" />
                        </Grid>
                        <Grid item xs={8}>
                            <TextField id="outlined-basic" name="nombre" value={ nombre } onChange={ handleInputChange } label="Nombre del cliente" fullWidth variant="outlined" />
                        </Grid>
                        <Grid item xs={2}>
                                <IconButton aria-label="search" size="large" onClick={ handleSearch }>
                                    <SearchIcon />
                                </IconButton>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={10}>
                    <div>
                        <TextField id="outlined-basic" name="direccion" value={ direccion } onChange={ handleInputChange } label="Dirección" fullWidth variant="standard" />
                    </div>
                    <div>
                        <Grid container spacing={2} columns={12} sx={{ mt:1 }}>
                            <Grid item xs={6}>
                                <TextField 
                                    id="outlined-basic"
                                    label="Celular"
                                    name="celular"
                                    value={ celular }
                                    onChange={ handleInputChange }
                                    variant="standard"                                    
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <TextField 
                                    id="outlined-basic"
                                    label="Precio por litro"
                                    name="precio"
                                    value={ precio }
                                    variant="outlined"
                                    InputProps={{
                                        startAdornment: (
                                          <InputAdornment position="start">
                                            <AttachMoneyIcon />
                                          </InputAdornment>
                                        ),
                                      }}
                                 />
                            </Grid>
                            <Grid item xs={3}>
                                <TextField 
                                    id="outlined-basic"
                                    label="Descuento"
                                    name="descuento"
                                    value={ descuento }
                                    onChange={ handleInputChange }
                                    variant="outlined"
                                    InputProps={{
                                        startAdornment: (
                                          <InputAdornment position="start">
                                            <AttachMoneyIcon />
                                          </InputAdornment>
                                        ),
                                      }}
                                />
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
            </Grid>
        </Box>
    )
}
