import React from 'react'
import { Box } from '@mui/system';
import { styled } from '@mui/material/styles';
import { BusquedaCliente } from './BusquedaCliente';
import { Divider } from '@mui/material';
import { TabItems } from './TabItems';
import { Verificacion } from './Verificacion';

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

export const VentasScreen = () => {
  return (
    <div>
      <Box position="flex" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />
        <BusquedaCliente />
        <br />
        <Divider />
        <TabItems />
        <Verificacion />
      </Box>
    </div>
  )
}
