import React, { useEffect, useState } from 'react'
import { Button, Grid } from '@mui/material'
import { Box } from '@mui/system'
import { styled } from '@mui/material/styles';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import { useDispatch, useSelector } from 'react-redux';
import { DataGrid } from '@mui/x-data-grid';
import moment from 'moment';
import { ModalVerificacion } from '../modales/ModalVerificacion';
import { abonoStartNew } from '../../actions/ventas';

const EnviarButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText('#043568'),
    backgroundColor: '#043568',
    '&:hover': {
        backgroundColor: '#064990',
    },
    height: 80,
    width: 180
}));

let metodoPagoId = 0;

const initialForm = {
    abono: 0,
    pendiente: 0,
    creditoDespues: 0
};

let InitialData= {};

const modalMessage = {
    header:'Registro de abono',
    body:'¿Estas seguro que deseas enviar a verificación este abono?'
}

export const TabAbono = () => {
    
    const dispatch = useDispatch();
    const { active } = useSelector(state => state.ventas)
    //const { data } = useSWR(`/venta/pendiente?clienteId=${activeCliente.id}`, { refreshInterval: 1000 });

    const { pipas } = useSelector(state => state.pipas);
    const { metodosPago } = useSelector(state => state.metodosPago);

    const [sendData, setSendData] = useState(false);
    const [data, setData] = useState(InitialData);
    const [metodoPago, setMetodoPago] = useState(['']);
    const [openModal, setOpenModal] = useState(false);
    const [selectionModel, setSelectionModel] = useState([]);
    const [rows, setRows] = useState([]);
    const [ formValues, setFormValues ] = useState(initialForm);
    const { abono, pendiente, creditoDespues } = formValues;

    useEffect(() => {
        const tableData = (data) => {
            console.log('refresh')
            return data.map(
              (e) => ({
                id: e.id,
                fecha: moment(e.createdAt).format('DD/MM/YYYY'),
                pipa: pipas.find(f => f.id === e.pipaId).nombre,
                venta: e.monto,
                litros: e.litrosVendidos,
                precioGas: e.precioGas,
                metodoPago: metodosPago.find(f => f.id === e.metodoPagoId).denominacion,
                montoPendiente: e.montoPendiente
              })
            )
          };

          if (active && pipas.length !== 0 && metodosPago.length !== 0) {      
            setRows(tableData(active));
          }

          if (sendData === true) {
            console.log('post abono');
            dispatch( abonoStartNew(data) );
            setData(InitialData);
            setFormValues(initialForm);
            setSendData(false);
        }
    }, [active, pipas, metodosPago, sendData, data, dispatch])

    const handleInputChange = ({ target }) => {
        setFormValues({
            ...formValues,
            [ target.name ]: target.value
        });

        if (target.name === "abono") {
            let pendiente = active.find(f => f.id === parseInt(selectionModel)).montoPendiente;
            setFormValues({
                abono: target.value,
                creditoDespues: pendiente - target.value
            });            
        }
    }

    const handleChangeTipoPago = (event) => {
        const {
            target: { value },
        } = event;
        setMetodoPago(
            typeof value === 'string' ? value.split(',') : value,
        );

        metodoPagoId = metodosPago.find(f => f.denominacion === value).id
    };

    const handleOpenModal = () => {
        setData({
            cantidad: parseFloat(abono),
            ventaId: selectionModel[0],
            metodoPagoId
        });
        setOpenModal(true)
        //console.log(selectionModel);
    };

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container columns={16} sx={{ mt: 0 }}>
            {/* <Alert severity="info" style={{ marginBottom: 8 }}>
        <code>selectionModel: {JSON.stringify(selectionModel)}</code>
      </Alert> */}
            <div style={{ display: 'table', tableLayout: 'fixed', width: '100%', height: 300 }}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        onSelectionModelChange={(selection) => {
                            if (selection.length > 1) {
                              const selectionSet = new Set(selectionModel);
                              const result = selection.filter((s) => !selectionSet.has(s));                    
                              setSelectionModel(result);
                              setFormValues( { pendiente:active.find(f => f.id === parseInt(result)).montoPendiente } );
                            } else {
                              setSelectionModel(selection);
                              setFormValues( { pendiente:active.find(f => f.id === parseInt(selection)).montoPendiente } );
                            }                            
                          }
                        }
                        selectionModel={selectionModel}
                        {...rows}
                    />
                </div>
                <Box sx={{ flexDirection: 'row', display: 'flex', mt:3 }}>                
                    <div>
                        <div>
                            Crédito pendiente
                        </div>
                        <TextField
                            id="standard-number"
                            type="number"
                            color="error"
                            name="pendiente"
                            value={ pendiente }
                            onChange={ handleInputChange }
                            focused
                            InputLabelProps={{
                                shrink: true,
                            }}
                            sx={{ ml: 1, width: 120 }}
                            variant="outlined"
                            size="small"
                        />
                    </div>

                    <div>
                        <div style={{marginLeft:60}}>
                            Abonar
                        </div>
                        <TextField
                            id="standard-number"
                            type="number"                            
                            name="abono"
                            value={ abono }
                            onChange={ handleInputChange }
                            InputLabelProps={{
                                shrink: true,
                            }}
                            sx={{ ml:5, mr:5, width: 100 }}
                            variant="standard"
                        />
                    </div>

                    {/* <FormLabel component="legend">labelPlacement</FormLabel> */}
                    <div>
                        <div>
                            Crédito despues de abono
                        </div>
                        <TextField
                            id="standard-number"
                            type="number"
                            name="creditoDespues"
                            value={ creditoDespues }
                            onChange={ handleInputChange }
                            color="error"
                            focused
                            InputLabelProps={{
                                shrink: true,
                            }}
                            sx={{ ml: 5, mr: 3, width: 120 }}
                            variant="outlined"
                            size="small"
                        />
                    </div>

                    <Box sx={{ minWidth: 90, ml: 2, mr: 3 }}>
                        <FormControl variant="standard" fullWidth>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={metodoPago}
                                onChange={handleChangeTipoPago}
                            >
                                {metodosPago.map((t) => (
                                    <MenuItem
                                        key={t.id}
                                        value={t.denominacion}
                                    >
                                        {t.denominacion}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Box>

                    <EnviarButton onClick={handleOpenModal} variant="contained" size="small">
                        Enviar a lista de verificación
                    </EnviarButton>
                </Box>
            </Grid>

            <ModalVerificacion openModal={openModal} setOpenModal={setOpenModal} setSendData={setSendData} modalMessage={modalMessage} />
        </Box>
    )
}

const columns = [
    { field: 'id', headerName: 'ID Venta', type: 'number', editable: true },
    {
      field: 'fecha',
      headerName: 'Fecha',
      type: 'date',
      width: 120,
      editable: true,
    },
    { field: 'pipa', headerName: 'Pipa', width: 130, editable: true },
    { field: 'venta', headerName: 'Venta', width: 120, editable: true },
    { field: 'litros', headerName: 'Litros', type: 'number', editable: true },
    { field: 'precioGas', headerName: 'Precio del gas', type: 'number', width: 150, editable: true },
    { field: 'metodoPago', headerName: 'Método de pago', width: 180, editable: true },
    { field: 'montoPendiente', headerName: 'Crédito pendiente', width: 180, editable: true },
  ];