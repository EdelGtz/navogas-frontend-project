import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Button, Grid, Paper, Typography } from '@mui/material'
import Box from '@mui/material/Box'
import { styled } from '@mui/material/styles';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';

import '../../styles/ventas.css';
import { pipasStartLoading } from '../../actions/pipas';
import { metodosPagoStartLoading } from '../../actions/metodosPago';
import { ventasStartRegister } from '../../actions/ventas';
import { ModalVerificacion } from '../modales/ModalVerificacion';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 50,
        },
    },
};

const EnviarButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText('#043568'),
    backgroundColor: '#043568',
    '&:hover': {
        backgroundColor: '#064990',
    },
    height:80,
    width:180
}));

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    backgroundColor: 'transparent',
    boxShadow: 'none',
}));

let showAnticipo = false;
let metodoPagoId = 0;
let pipaId = 0;
let InitialData= {};

const initialForm = {
    litrosVendidos: 0,
    monto: 0,
    tipoVenta: '',
    anticipo: 0
};

const modalMessage = {
    header:'Registro de venta',
    body:'¿Estas seguro que deseas enviar a verificación esta venta?'
}

export const TabVentas = () => {

    const dispatch = useDispatch();

    const { pipas } = useSelector(state => state.pipas);
    const { metodosPago } = useSelector(state => state.metodosPago);
    const { activeCliente } = useSelector(state => state.clientes)
    const { precio: precioGas } = useSelector(state => state.gas)

    const [openModal, setOpenModal] = useState(false);
    const [precio, setPrecioGas] = useState()
    const [pipa, setPipa] = useState(['']);
    const [metodoPago, setMetodoPago] = useState(['']);
    const [sendData, setSendData] = useState(false);
    const [data, setData] = useState(InitialData);

    const [ formValues, setFormValues ] = useState(initialForm);
    const { monto, litrosVendidos, tipoVenta, anticipo } = formValues;

    useEffect(() => {
        if (activeCliente) {
            let precio = precioGas - activeCliente.descuento;
            setPrecioGas(precio.toFixed(2));            
        }
        dispatch( pipasStartLoading() );
        dispatch( metodosPagoStartLoading() );

        if (sendData === true) {
            console.log('post venta');
            dispatch( ventasStartRegister(data) );
            setData(InitialData);
            setFormValues(initialForm);
            setSendData(false);
        }
    }, [activeCliente, precioGas, setPrecioGas, dispatch, data, sendData])
        
    const handleOpen = () => {
        //console.log(litrosVendidos)
        setData({
            litrosVendidos: parseFloat(litrosVendidos),
            tipoVenta,
            anticipo: isNaN(parseFloat(anticipo)) ? 0 : parseFloat(anticipo),
            pipaId,
            clienteId: activeCliente.id,
            metodoPagoId
        });
        setOpenModal(true);
    }

    const handleChangePipa = (event) => {
        const {
            target: { value },
        } = event;
        setPipa(
            typeof value === 'string' ? value.split(',') : value,
        );

        pipaId = pipas.find(f => f.nombre === value).id
    };

    const handleChangeTipoPago = (event) => {
        const {
            target: { value },
        } = event;
        setMetodoPago(
            typeof value === 'string' ? value.split(',') : value,
        );

        metodoPagoId = metodosPago.find(f => f.denominacion === value).id
    };

    const handleInputChange = ({ target }) => {
        setFormValues({
            ...formValues,
            [ target.name ]: target.value
        });

        if (target.value === "CREDITO") {
            showAnticipo = true;
        }

        if (target.name ===  "litrosVendidos") {
            setFormValues({
                monto: parseFloat(target.value) * parseFloat(precio),
                litrosVendidos: parseFloat(target.value)
            });
         }
    }

    return (
        <Box sx={{ flexGrow: 1 }}>
            <div>
                <Grid container columns={16}>
                    <div>
                        <Typography variant="subtitle1" gutterBottom component="div">
                            Pipa
                        </Typography>
                        <FormControl sx={{ m: 0, width: 150 }}>
                            <Select
                                labelId="demo-multiple-name-label"
                                id="demo-multiple-name"
                                value={pipa}
                                onChange={handleChangePipa}
                                MenuProps={MenuProps}
                            >
                                {pipas.map(( p ) => (
                                    <MenuItem
                                        key={p.id}
                                        value={p.nombre}
                                    >
                                        {p.nombre}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </div>
                </Grid>
            </div>
            <Grid container columns={16} sx={{ mt: 5 }}>
                <Box sx={{ flexDirection: 'row', display: 'flex' }}>
                    <TextField
                        id="standard-number"
                        label="Litros"
                        type="number"
                        name="litrosVendidos"
                        value={ litrosVendidos }
                        onChange={ handleInputChange }
                        InputLabelProps={{
                            shrink: true,
                        }}
                        sx={{ width: 100 }}
                        variant="standard"
                    />
                    <Typography variant="h6" gutterBottom component="div" sx={{ ml: 3, mr: 3, mt: 1 }}>X</Typography>
                    <TextField
                        id="standard-number"
                        label="Precio"
                        type="number"
                        name="precio"
                        value={ precio }
                        InputLabelProps={{
                            shrink: true,
                        }}
                        sx={{ width: 100 }}
                        variant="standard"
                    />
                    {/* <FormLabel component="legend">labelPlacement</FormLabel> */}
                    <TextField
                        id="standard-number"
                        label="Venta total"
                        type="number"
                        name="monto"
                        value={ monto }
                        onChange={ handleInputChange }
                        color="success"
                        focused
                        InputLabelProps={{
                            shrink: true,
                        }}
                        sx={{ ml: 3, mr: 3, width: 120 }}
                        variant="outlined"
                        size="small"
                    />

                    <div>
                        <FormControl component="fieldset">
                            <RadioGroup row aria-label="position" name="position" defaultValue="top" value={tipoVenta} onChange={handleInputChange}>
                                <FormControlLabel
                                    name="tipoVenta"
                                    value="CONTADO"
                                    control={<Radio />}
                                    sx={{
                                        '& .MuiSvgIcon-root': {
                                          fontSize: 17,
                                        },
                                      }}
                                    label="Contado"
                                />
                                <FormControlLabel 
                                    name="tipoVenta"
                                    value="CREDITO"
                                    control={<Radio />}
                                    sx={{
                                        '& .MuiSvgIcon-root': {
                                          fontSize: 17,
                                        },
                                      }}
                                    label="Crédito"
                                />
                            </RadioGroup>
                        </FormControl>
                        <div style={{visibility: showAnticipo ? 'visible' : 'hidden' }}>
                            <div style={{ float: "right" }}>
                                <Item>
                                    <TextField
                                        id="standard-number"
                                        type="number"
                                        name="anticipo"
                                        value={ anticipo }
                                        onChange={ handleInputChange }
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        sx={{ width: 100 }}
                                        variant="standard"
                                    />
                                </Item>
                                <Item>
                                    Anticipo
                                </Item>
                            </div>
                        </div>
                    </div>

                    <Box sx={{ minWidth: 90, ml: 2, mr: 3 }}>
                        <FormControl variant="standard" fullWidth>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={metodoPago}
                                onChange={handleChangeTipoPago}
                            >
                                {metodosPago.map((t) => (
                                    <MenuItem
                                        key={t.id}
                                        value={t.denominacion}
                                    >
                                        {t.denominacion}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Box>

                    <EnviarButton variant="contained" size="small" onClick={handleOpen}>
                        Enviar a lista de verificación
                    </EnviarButton>
                </Box>
            </Grid>
            <ModalVerificacion openModal={openModal} setOpenModal={setOpenModal} setSendData={setSendData} modalMessage={modalMessage} />
        </Box>
    )
}