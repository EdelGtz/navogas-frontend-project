import React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { DataGrid } from '@mui/x-data-grid';
import { Alert, Button, Typography } from '@mui/material';
import { styled } from '@mui/system';


const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1150,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const SeleccionarButton = styled(Button)(({ theme }) => ({
    //color: theme.palette.getContrastText('#043568'),
    backgroundColor: '#043568',
    '&:hover': {
        backgroundColor: '#064990',
    },
    height:35,
    width:120
}));

export const ModalAbonar = ({ openModal, setOpenModal }) => {

    const handleClose = () => setOpenModal(false);

    const handleRowSelected = (e) => {
        console.log(e.id)
    }

    return (
        <div>
            <Modal
                open={openModal}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={modalStyle}>

                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Selecciona la venta donde quieras aplicar el abono
                    </Typography>

                    <div style={{ width: '100%' }}>
                        <Alert severity="info" style={{ marginBottom: 8 }}>
                            {/* <code>selectionModel: {JSON.stringify(selectionModel)}</code> */}
                        </Alert>
                        <div style={{ height: 400, width: '100%' }}>
                            <DataGrid
                                rows={rows}
                                columns={columns}
                                onRowClick={ handleRowSelected }
                            />
                        </div>
                    </div>
                    <div style={{ marginTop:5, float: "right" }}>
                        <SeleccionarButton variant="contained" size="small">
                            Seleccionar
                        </SeleccionarButton>
                    </div>

                </Box>
            </Modal>
        </div>
    );
}

const columns = [
    {
        field: 'fecha',
        headerName: 'Fecha',
        type: 'date',
        width: 100,
        editable: true,
    },
    { field: 'pipa', headerName: 'Pipa', width: 80, editable: true },
    { field: 'venta', headerName: 'Venta', width: 100, editable: true },
    { field: 'litros', headerName: 'Litros', type: 'number', editable: true },
    { field: 'precio', headerName: 'Precio', type: 'number', editable: true },
    {
        field: 'tipoVenta',
        headerName: 'Tipo de venta',
        width: 130,
        editable: true
    },
    { field: 'metodoPago', headerName: 'Método de pago', width: 130, editable: true },
    { field: 'anticipo', headerName: 'Anticipo', width: 130, editable: true },
    { field: 'montoPendiente', headerName: 'Monto pendiente', width: 150, editable: true },
];

const rows = [
    {
        id: "1",
        fecha: "01/09/2021",
        pipa: "Pipa #2",
        venta: "$3,272.00",
        litros: 250,
        precio: 13.10,
        tipoVenta: "Contado",
        metodoPago: "Efectivo",
        anticipo: "$3,272.00",
        montoPendiente: "0",
    },
    {
        id: "4",
        fecha: "22/09/2021",
        pipa: "Pipa #1",
        venta: "$2,620.00",
        litros: 200,
        precio: 13.10,
        tipoVenta: "Crédito",
        metodoPago: "Transferencia",
        anticipo: "$1,000.00",
        montoPendiente: "$1,620.00",
    },
    {
        id: "5",
        fecha: "01/09/2021",
        pipa: "Pipa #4",
        venta: "$3,272.00",
        litros: 250,
        precio: 13.10,
        tipoVenta: "Crédito",
        metodoPago: "Sin Pago",
        anticipo: "0",
        montoPendiente: "$3,272.00",
    },
];