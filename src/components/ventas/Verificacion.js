import React, { useCallback, useEffect, useState } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { Button, FormControl, MenuItem, Select, Typography } from '@mui/material';
import useSWR from 'swr';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { sendToAutorizacion } from '../../actions/ventas';
import Swal from 'sweetalert2';

function renderEditableCell(params) {
  return <EditCell {...params} />;
}

function EditCell(props) {
  const { value } = props;

  return (
    <div>
      <FormControl>
        <Select
          value={value || ''}
          onChange={(e) => console.log("changed to ", e.target.value)}
        >
          {["Contado", "Crédito"].map((val, ind) =>
            <MenuItem key={ind} value={val}>{val}</MenuItem>
          )}
        </Select>
      </FormControl>
    </div>
  );
}

export const Verificacion = () => {

  const dispatch = useDispatch();

  const { data } = useSWR('/venta?estadoOperacion=VERIFICACION', { refreshInterval: 1000 });
  const { pipas } = useSelector(state => state.pipas);
  const { metodosPago } = useSelector(state => state.metodosPago);

  const [editRowsModel, setEditRowsModel] = useState({});
  const [selectionModel, setSelectionModel] = useState([]);
  const [rows, setRows] = useState([]);

  const handleEditRowsModelChange = useCallback((model) => {
    setEditRowsModel(model);
  }, []);

  const handleUpdate = (e) => {
    console.log(e);
  }

    
  useEffect(() => {
    const tableData = (data) => {
      return data.map(
        (e) => ({
          id: e.id,
          fecha: moment(e.createdAt).format('DD/MM/YYYY'),
          pipa: pipas.find(f => f.id === e.pipaId).nombre,
          venta: e.monto,
          litros: e.litrosVendidos,
          precio: e.precioGas,
          tipoVenta: e.tipoVenta,
          metodoPago: metodosPago.find(f => f.id === e.metodoPagoId).denominacion,
          anticipo: e.tipoVenta === 'CONTADO' ? 0 : e.abonos.find((abono) => abono.anticipo)?.cantidad || 0,
          montoPendiente: e.tipoVenta === 'CONTADO' ? 0 : e.monto - (e.abonos.find((abono) => abono.anticipo)?.cantidad || 0)
        })
      )
    };
    if (data !== undefined && pipas.length !== 0 && metodosPago.length !== 0) {      
      setRows(tableData(data));
    }
  }, [data, pipas, metodosPago])

  if (!data) {
    return <h1>Cargando...</h1>
  }

  const handleSend = () => {
    //console.log(selectionModel.length)
    if (selectionModel.length !== 0) {
      const data = {
        ids: selectionModel,
        estadoOperacion: "AUTORIZACION"
      }
      
      dispatch( sendToAutorizacion(data) );
    } else {
      console.log('object')
      Swal.fire('Error', 'Debe seleccionar al menos una venta', 'error')
    }
    
  }

  //https://codesandbox.io/s/material-demo-forked-w514o?file=/demo.js
  return (
    <div style={{ width: '100%' }}>
      <Typography variant="h5" gutterBottom component="div">
        Verificaciones Pendientes
      </Typography>
      <Button variant="contained" onClick={ handleSend }>Enviar a autorización</Button>
      {/* <Alert severity="info" style={{ marginBottom: 8 }}>
        <code>editRowsModel: {JSON.stringify(editRowsModel)}</code>
      </Alert> */}
      {/* <Alert severity="info" style={{ marginBottom: 8 }}>
        <code>selectionModel: {JSON.stringify(selectionModel)}</code>
      </Alert> */}
      <div style={{ display: 'table', tableLayout: 'fixed', width: '100%', height: 400 }}>
        <DataGrid
          rows={rows}
          columns={columns}
          editRowsModel={editRowsModel}
          onCellEditCommit={handleUpdate}
          onEditRowsModelChange={handleEditRowsModelChange}
          checkboxSelection
          disableSelectionOnClick
          onSelectionModelChange={(newSelectionModel) => {
            setSelectionModel(newSelectionModel);
          }}
          selectionModel={selectionModel}
          {...rows}
        />
      </div>
    </div>
  );
}

const columns = [
  {
    field: 'fecha',
    headerName: 'Fecha',
    type: 'date',
    width: 180,
    editable: true,
  },
  { field: 'pipa', headerName: 'Pipa', width: 180, editable: true },
  { field: 'venta', headerName: 'Venta', width: 180, editable: true },
  { field: 'litros', headerName: 'Litros', type: 'number', editable: true },
  { field: 'precio', headerName: 'Precio', type: 'number', editable: true },
  {
    field: 'tipoVenta',
    headerName: 'Tipo de venta',
    //renderCell: renderEditableCell,
    renderEditCell: renderEditableCell,
    width: 180,
    editable: true
  },
  { field: 'metodoPago', headerName: 'Método de pago', width: 180, editable: true },
  { field: 'anticipo', headerName: 'Anticipo', width: 180, editable: true },
  { field: 'montoPendiente', headerName: 'Monto pendiente', width: 180, editable: true },
];
