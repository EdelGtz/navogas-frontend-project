import React from 'react'
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useDispatch, useSelector } from 'react-redux'

import { useForm } from '../../hooks/useForm'
import { CircularProgress, InputAdornment } from '@mui/material';
import { green } from '@mui/material/colors';
import LockIcon from '@mui/icons-material/Lock';

import logo from '../../assets/navogas.svg';
import poweredBy from '../../assets/powered-by-eddno.svg';
import EmailIcon from '@mui/icons-material/Email';
import { startLogin } from '../../actions/auth';

function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" href="https://eddno.com/">
                DigitaLapps
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const theme = createTheme();

//https://github.com/search?q=react+gallery&type=Repositories

export const LoginScreen = () => {

    const dispatch = useDispatch();
    const { loading } = useSelector(state => state.ui)

    const [formValues, handleInputChange] = useForm({
        username: '',
        password: ''
    });

    const { username, password } = formValues;

    const handleLogin = (e) => {
        e.preventDefault();
        dispatch( startLogin( username, password ) );
    };


    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs" className="animate__animated animate__fadeIn animate_faster">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <img style={{ marginBottom: "10px" }} src={logo} alt={"logo"} />
                    <Typography sx={{mt:2}} component="h1" variant="h5">
                        Iniciar Sesión
                    </Typography>
                    <Box component="form" onSubmit={handleLogin} noValidate sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="Introduzca su usuario"
                            name="username"
                            autoComplete="off"
                            autoFocus
                            value={username}
                            onChange={handleInputChange}
                            InputProps={{
                                startAdornment: (
                                  <InputAdornment position="start">
                                    <EmailIcon />
                                  </InputAdornment>
                                ),
                              }}
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Introduzca su contraseña"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            value={password}
                            onChange={handleInputChange}
                            InputProps={{
                                startAdornment: (
                                  <InputAdornment position="start">
                                    <LockIcon />
                                  </InputAdornment>
                                ),
                              }}
                        />

                        <Box sx={{ m: 0, position: 'relative' }}>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                disabled={loading}
                                sx={{ mt: 3, mb: 2, backgroundColor: "#043568"}}
                            >
                                Acceder
                            </Button>
                            {loading && (
                                <CircularProgress
                                    size={24}
                                    sx={{
                                        color: green[500],
                                        position: 'absolute',
                                        top: '50%',
                                        left: '50%',
                                        marginTop: '-12px',
                                        marginLeft: '-12px',
                                    }}
                                />
                            )}
                        </Box>

                    </Box>
                </Box>
                <Copyright sx={{ mt: 6, mb: 0 }} />
                <div style={{textAlign: "right"}}>
                    <img style={{ maxWidth: "38%", height:"auto"}} src={poweredBy} alt={"poweredBy"} />
                </div>
            </Container>
        </ThemeProvider>
    )
}
