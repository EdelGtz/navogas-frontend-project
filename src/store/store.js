import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk';

import { authReducer } from '../reducers/authReducer';
import { clientesReducer } from '../reducers/clientesReducer';
import { gasReducer } from '../reducers/gasReducer';
import { pipasReducer } from '../reducers/pipasReducer';
import { uiReducer } from '../reducers/uiReducer';
import { metodosPagoReducer } from '../reducers/metodosPagoReducer';
import { ventasReducer } from '../reducers/ventasReducer';

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducers = combineReducers({
    auth: authReducer,
    ui: uiReducer,
    gas: gasReducer,
    clientes: clientesReducer,
    pipas: pipasReducer,
    metodosPago: metodosPagoReducer,
    ventas: ventasReducer,
})

export const store = createStore(
    reducers,
    composeEnhancers(
        applyMiddleware( thunk )
    )
);